<?php

namespace App\Providers;

use App\BusinessRu\TokenManager;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class TokenManagerServiceProvider extends ServiceProvider
{
    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        $this->app->singleton(TokenManager::class, function ($app) {
            return new TokenManager($app['config']['services']['bussiness_ru']);
        });
    }
}
