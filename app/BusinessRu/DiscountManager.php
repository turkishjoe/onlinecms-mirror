<?php
/**
 * TODO:
 * Created by PhpStorm.
 * User: prog12
 * Date: 10.07.18
 * Time: 1:00
 */

namespace App\BusinessRu;


use Faker\Factory;
use Illuminate\Support\Facades\Log;

class DiscountManager
{
    /**
     * @var TokenManager
     */
    protected $tokenManager;


    public function __construct(TokenManager $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    public function getDiscount($partnerId)
    {
        $discount = 0;
        $result = $this->tokenManager->request('get', 'discountcards', [
            'partner_id'=>$partnerId
        ]);

        foreach ($result as $discountElement)
        {
           $discount += $discountElement['current_discount_value'];
        }

        return $discount;
    }

}