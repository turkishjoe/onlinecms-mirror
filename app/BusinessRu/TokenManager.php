<?php
/**
 * TODO:
 * Created by PhpStorm.
 * User: prog12
 * Date: 07.07.18
 * Time: 4:37
 */

namespace App\BusinessRu;


class TokenManager
{
    /**
     * @var Client
     */
    protected $api;

    /**
     * @var
     */
    protected $config;

    public function __construct($config)
    {
        $this->config = $config;
        $this->initApi();
    }

    public function request($action, $model, $params = array())
    {
        $result =  $this->api->request($action, $model, $params);

        $this->saveToken($result);

        if($result['status'] !== 'ok')
        {
            throw new \RuntimeException("Unauthorized" . json_encode($result));
        }

        return $result['result'];
    }

    protected function initApi()
    {
        $token = $this->getToken();
        $this->api = new Client($this->config['app_id'], $token,  $this->config['address']);
        $this->api->setSecret($this->config['app_secret']);

        if(empty($token))
        {
            $this->saveToken($this->api->repair());
        }

        return $this->api;
    }

    protected function saveToken($result)
    {
        if(!empty($result['token']))
        {
            file_put_contents('token.txt', $result['token']);
        }

        return $this;
    }

    protected function getToken()
    {
        return file_get_contents('token.txt') ?? '';
    }
}