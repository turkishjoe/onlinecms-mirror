<?php
/**
 * TODO:
 * Created by PhpStorm.
 * User: prog12
 * Date: 10.07.18
 * Time: 1:00
 */

namespace App\BusinessRu;


use Faker\Factory;

class DataGenerator
{
    /**
     * @var TokenManager
     */
    protected $tokenManager;

    /**
     * @var Factory
     */
    protected $faker;

    protected $categories = [];
    protected $kinds = [];
    protected $sizes = [];
    protected $regions = [];
    protected $empolyes = [];
    protected $partnerStatus = [];
    protected $legacyTypes = [];
    protected $partners = [];


    public function __construct(TokenManager $tokenManager)
    {
        $this->tokenManager = $tokenManager;
        $this->faker = Factory::create();
        $this->initData();
    }

    public function generateCreateClient($isEdit = false)
    {
        if($isEdit)
        {
            $this->partners = $this->tokenManager->request('get', 'partners');
        }

        for($i = 0; $i < 10; $i++) {
            $data = [
                'organization_type_id' => mt_rand(1, 8),
                'legacy_type_id'=>$this->getRandomId($this->legacyTypes),
                'name'=>$this->faker->name,
                'full_name'=>$this->faker->name,
                'customer'=>$this->faker->boolean,
                'supplier'=>$this->faker->boolean,
                'competitor'=>$this->faker->boolean,
                'partner'=>$this->faker->boolean,
                'potential'=>$this->faker->boolean,
                'inn'=>$this->faker->randomDigit,
                'kpp'=>$this->faker->randomDigit,
                'ogrn'=>$this->faker->randomDigit,
                'okpo'=>$this->faker->name,
                'address_legal'=>$this->faker->name,
                'address_actual'=>$this->faker->name,
                'note'=>$this->faker->name,
                'status_id'=>$this->getRandomId($this->partnerStatus),
                'responsible_employee_id'=>$this->getRandomId($this->empolyes),
                'category_id'=>$this->getRandomId($this->categories),
                'kind_id'=>$this->getRandomId($this->kinds),
                'code'=>$this->faker->randomDigit
            ];

            if($isEdit)
            {
                $data['id'] = $this->getRandomId($this->partners);
            }

            $this->tokenManager->request('post', 'partners', $data);
        }
    }

    protected function initData()
    {
        $this->legacyTypes = $this->tokenManager->request('get', 'legacytypes');
        $this->partnerStatus = $this->tokenManager->request('get', 'partnerstatus');

        $this->empolyes = $this->tokenManager->request('get', 'employees');
        $this->categories = $this->getIdNameEntity('organizationcategories');
        $this->kinds = $this->getIdNameEntity( 'organizationkinds');
        $this->sizes =  $this->getIdNameEntity( 'organizationsizes');
        $this->regions = $this->tokenManager->request( 'get','organizationregions');
    }

    protected function getIdNameEntity($apiEntityName, $countData = 5)
    {
        $result = $this->tokenManager->request('get', $apiEntityName);

        if(empty($result))
        {
            for($i = 0; $i < $countData; $i++)
            {
                $this->tokenManager->request('post', $apiEntityName, [
                    'name'=>$this->faker->company
                ]);
            }

            $result = $this->tokenManager->request('get', $apiEntityName);
        }

        return $result;
    }

    protected function getEmployers($countData = 5)
    {
        $apiEntityName = 'employees';
        $result = $this->tokenManager->request('get', $apiEntityName);

        if(empty($result))
        {
            for($i = 0; $i < $countData; $i++)
            {
                $this->tokenManager->request('post', $apiEntityName, [
                    'first_name'=>$this->faker->name,
                    'last_name'=>$this->faker->name,
                    'middle_name'=>$this->faker->name
                ]);
            }

            $result = $this->tokenManager->request('get', $apiEntityName);
        }

        return $result;
    }

    protected function getRandomId($array)
    {
        return $this->faker->randomElement($array)['id'];
    }
}