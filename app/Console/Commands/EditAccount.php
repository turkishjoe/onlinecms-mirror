<?php

namespace App\Console\Commands;

use App\BusinessRu\DataGenerator;
use App\BusinessRu\TokenManager;
use Illuminate\Console\Command;

class EditAccount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:edit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $dataGenerator;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DataGenerator $dataGenerator)
    {
        parent::__construct();

        $this->dataGenerator = $dataGenerator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->dataGenerator->generateCreateClient(true);
        echo 'ok' . PHP_EOL;
    }

}
