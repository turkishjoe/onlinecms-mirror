<?php

namespace App\Console\Commands;

use App\BusinessRu\DataGenerator;
use App\BusinessRu\DiscountManager;
use App\BusinessRu\TokenManager;
use Illuminate\Console\Command;
use SebastianBergmann\CodeCoverage\Report\PHP;

class GetDiscount extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'account:discount {partnerId}' ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var DiscountManager
     */
    protected $discountManager;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(DiscountManager $discountManager)
    {
        parent::__construct();

        $this->discountManager = $discountManager;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      echo  $this->discountManager->getDiscount($this->argument('partnerId')) . '%' . PHP_EOL;
    }

}
